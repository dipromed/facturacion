from odoo import models,fields,api

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    _description = 'Sale Order Line'
    
    image_small = fields.Binary(
        'Product Image', related='product_id.image_small')
    
    def onchange_product_id(self, cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=False, fiscal_position_id=False, date_planned=False,
            name=False, price_unit=False, state='draft', context=None):
        context = context or {}

        res = super(SaleOrderLine, self).onchange_product_id(cr, uid, ids, pricelist_id, product_id, qty, uom_id,
            partner_id, date_order=date_order, fiscal_position_id=fiscal_position_id, date_planned=date_planned,
            name=name, price_unit=price_unit, state='draft', context=context)

        product_obj = self.pool.get('product.product')
        product_obj = product_obj.browse(cr, uid, product_id, context=context)

        res['value'].update({'image_small': product_obj.image_small or False})
        return res