{
    'name': 'Product Image on Sale Order and Invoice Order',
    'version': '1.0',
    'category': 'Sales',
    'author': 'Cubicle Technolabs',
    'summary': 'This module provides a functionality to display product image in sale order and account invoice with its qweb report.',
    'description': "",
    'license': 'OPL-1',
    'depends': [
        'sale_management'
    ],
    'data': [
        'views/sale_order_line_view.xml',
        'views/sale_order_qweb_report.xml',
        'views/account_invoice_line_view.xml',
        'views/account_invoice_qweb_report.xml',       
    ],
    'images': ['static/description/cover_image.jpg'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
