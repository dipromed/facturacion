# -*- coding: utf-8 -*-

from odoo import models, fields, api
import json
import requests
from suds.client import Client


class mycompany(models.Model):
    _inherit = 'res.company'

    document_number = fields.Char(
        string='Nro Documento',
        size=64,
    )

    document_type_id = fields.Selection([('rut', 'R.U.T.'),('cedula', 'Cédula'),('dni', 'DNI'), ('tax', 'Tax ID'),('pasaporte', 'Pasaporte')])    
    company_activities_ides = fields.Many2many('partner.activities','partner_activities_partners2', 'partner_id','activity_id', string="Actividades Económicas", select='True')  
    glosagiro = fields.Char(string='Glosa de giro')    
    dtemail = fields.Char(string='DTE Email')    
    resolucionsii = fields.Char(string='Número resolución exenta SII')    
    fecharesolucion = fields.Char(string='Fecha resolución exenta SII')    
    oficinasii = fields.Char(string='Oficina regional SII')    

    @api.onchange('document_number')
    def onchange_document(self):
        if self.document_number:
            api_url_base = 'https://siichile.herokuapp.com/consulta?rut='
            headers = {'Content-Type': 'application/json'}
            api_url = api_url_base+self.document_number
            response = requests.get(api_url, headers=headers)
            if response.status_code == 200:
                valores = json.loads(response.content.decode('utf-8'))
                self.document_number = valores['rut']

                giros = valores['actividades']
                self.company_activities_ides = [(6, 0, [])] 
                
                i=1
                for giro in giros:
                    ac = self.env['partner.activities'].search([('code','=',str(giro['codigo']))])
                    self.company_activities_ides +=ac                                
                    i = i+1



#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100