# -*- coding: utf-8 -*-

from odoo import models, fields, api

class presupuesto(models.Model):
    _inherit = 'purchase.order'
    shipvia = fields.Selection(string='Envío por', selection=[('BySeaCargo', 'Carga Marítima'),('ByAirCargo', 'Carga Aérea'),('ByTruckCargo', 'Carga Terrestre'),('ByRailCargo', 'Carga Ferroviaria' )])
    acquisitioncountry = fields.Many2one('res.country', string='País de Adquisición', help='Seleccione País de adquisición')
    eschileno = fields.Boolean(string="is show faculty")


    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            if self.partner_id.country_id.name == 'Chile':
                self.eschileno = True
                self.currency_id = self.partner_id.property_purchase_currency_id
            else:
                self.eschileno = False
                self.currency_id = self.partner_id.property_purchase_currency_id

