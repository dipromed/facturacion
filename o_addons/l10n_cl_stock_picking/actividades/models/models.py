# -*- coding: utf-8 -*-

from odoo import models, fields, api
import json
import requests
from suds.client import Client

mainact=[]
texto  = ''
texto2 = ''
texto3 = ''
codiso = ''
#mainact = ["Actividades de programación informática"; "Otras actividades de servicios personales n.c.p."]

class PartnerActivities(models.Model):
    _name = 'partner.activities'
    _description = 'SII Economical Activities'

    code = fields.Char(
            string='Codigo',
            required=True,
            translate=True,
        )
    name = fields.Char(
            string='Nombre Actividad',
            required=True,
            translate=True,
        )
    vat_affected = fields.Selection(
            (
                ('True', 'Si'),
                ('False', 'No'),
                ('ND', 'ND'),
            ),
            string='Afecta',
            required=True,
            translate=True,
            default='SI',

        )

    tax_category = fields.Selection(
            (
                ('1', 'Primera'),
                ('2', 'Segunda'),
                ('ND', 'ND'),
            ),
            string='Categoria Imp.',
            required=True,
            translate=True,
            default='1',
        )

    active = fields.Boolean(
            string='Activo',
            help="Allows you to hide the activity without removing it.",
            default=True,
        )

class actividades(models.Model):
    _inherit='res.partner'

    document_number = fields.Char(
        string='Nro Documento',
        size=64,
    )

    document_type_id = fields.Selection([('rut', 'R.U.T.'),('cedula', 'Cédula'),('dni', 'DNI'), ('tax', 'Tax ID'),('pasaporte', 'Pasaporte')])    
    partner_activities_ides = fields.Many2many('partner.activities','partner_activities_partners2', 'partner_id','activity_id', string="Actividades Económicas", select='True')  
    main_activity = fields.Many2one('partner.activities', string="Acteco Principal")  

    def check_vat_cl (self,rut ):
        api_url_base = 'https://siichile.herokuapp.com/consulta?rut='
        headers = {'Content-Type': 'application/json'}
        body, vdig = '', ''            
        rut = self.document_number
        losids = []
        if rut:
            largo = len(self.document_number)
            if largo > 7: 
                rut = rut.replace('-','',1).replace('.','',2)
                body, vdig = rut[:-1], rut[-1].upper()
                try:
                    vali = list(range(2,8)) + [2,3]
                    operar = '0123456789K0'[11 - (sum([int(digit)*factor for digit, factor in zip(body[::-1],vali)]) % 11)]
                    if operar == vdig:
                        api_url = api_url_base+self.document_number
                        response = requests.get(api_url, headers=headers)
                        if response.status_code == 200:
                            valores = json.loads(response.content.decode('utf-8'))
                            self.name = valores['razon_social'].title()
                            self.document_number = valores['rut']
                            texto  = valores['rut']
                            texto2 = texto.replace('.','')
                            texto3 = texto2[0:len(texto2)-2]
                            if int(texto3) > 47999999:
                                self.document_type_id = 'rut'
                                self.is_company=True
                            elif int(texto3) < 48000000:
                                self.document_type_id = 'cedula'
                                self.is_company=False

                            giros = valores['actividades']
                            self.partner_activities_ides = [(6, 0, [])] 
                            if not self.main_activity:
                                self.main_activity = [(6, 0, [])] 
                            
                            i=1
                            for giro in giros:
                                ac = self.env['partner.activities'].search([('code','=',str(giro['codigo']))])
                                elCodigo = self.env['partner.activities'].search([('code','=',str(giro['codigo']))]).code
                                elNombre = self.env['partner.activities'].search([('code','=',str(giro['codigo']))]).name
                                self.partner_activities_ides +=ac                                
                                i = i+1
                            
                        else:
                            print('None')

                        return True
                    else:
                        self.document_number = ''
                        return False
                except IndexError:
                    return False            
    
    @api.onchange('document_number')
    def onchange_document(self):
        if self.document_number:
            if not self.country_id:
                self.document_number = ''
                return {
                    'warning': {
                        'title': 'Falta Pais','message': 'Por favor seleccionar Pais o seleccionar "tipo de documento" en caso de Chilenos',
                        }
                    }            
            else:

                if self.document_number:
                    rut = self.document_number
                    rut = rut.replace('-','',1).replace('.','',2)
                    body, vdig = rut[:-1], rut[-1].upper()

                    texto  = self.document_number
                    texto2 = texto.replace('.','')
                    texto3 = texto2[0:len(texto2)-2]

                    if self.country_id:
                        if self.env['res.country'].search([('id','=',str(self.country_id.id))]).name == 'Chile':
                            if int(texto3) > 47999999:
                                self.document_type_id = 'rut'
                                self.is_company=True
                            elif int(texto3) < 48000000:
                                self.document_type_id = 'cedula'
                                self.is_company=False

                            if len(self.document_number)>=8:
                                if not self.check_vat_cl(self.document_number):
                                    return {
                                        'warning': {
                                            'title': 'Rut Erróneo','message': 'Rut Erróneo',
                                            }
                                        }
                        else:
                            codiso = self.env['res.country'].search([('id','=',str(self.country_id.id))]).code
                            self.document_number = self.document_number+codiso
                    
    @api.onchange('document_type_id')
    def onchange_document_type(self):
        if self.country_id:            
            if self.env['res.country'].search([('id','=',str(self.country_id.id))]).name == 'Chile':
                codiso = ''
            else:
                codiso = self.env['res.country'].search([('id','=',str(self.country_id.id))]).code
                print(codiso)

        if self.document_type_id:
            if self.document_type_id == 'rut':
                self.is_company=True
                self.country_id = self.env['res.country'].search([('name','=','Chile')]).id
            elif self.document_type_id == 'tax':
                self.is_company=True
            elif self.document_type_id == 'cedula':
                self.is_company=False
                self.country_id = self.env['res.country'].search([('name','=','Chile')]).id
            elif self.document_type_id == 'dni':
                self.is_company=False
            elif self.document_type_id == 'pasaporte':
                self.is_company=False
    
    @api.onchange('country_id')
    def onchange_country_id(self):
        self.document_number = ''



