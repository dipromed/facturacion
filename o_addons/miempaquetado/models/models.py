# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class miempaquetado(models.Model):
#     _name = 'miempaquetado.miempaquetado'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class miempaquetado(models.Model):
    _inherit = 'product.packaging'
    alto = fields.Float(string='Alto')
    ancho = fields.Float(string='Ancho')
    largo = fields.Float(string='Largo')
    peso = fields.Float(string='Peso Bruto (en Kg)')


