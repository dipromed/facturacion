# -*- coding: utf-8 -*-
{
    'name': "actividades",

    'summary': "Manejo de Actividades economicas segun SII en Chile",

    'description':" Manejo de Actividades economicas segun SII en Chile",

    'author': "Ivan Hills",
    'website': "https://www.dipromed.cl",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'partner',
    'version': '0.2',

    # any module necessary for this one to work correctly
    'depends': ['base','contacts'],

    # always loaded
    'data': [
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}