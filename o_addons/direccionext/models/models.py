# -*- coding: utf-8 -*-

from odoo import models, fields, api
import json
import requests 
import phonenumbers
from suds.client import Client

class Comunas(models.Model):
    _name="comunas.provincias"
    comuna = fields.Char(string='Comuna')
    provincia = fields.Char(string='Provincia')
    region = fields.Char(string='Region')

class PdfEmailPartner(models.Model):
    _name="res.partner.dteemail"
    _rec_name='email'
    email = fields.Char('Correos Facturacion')
    tipo = fields.Selection([('pdf', 'PDFComuna'),('xml', 'XML')])
    partner_id = fields.Many2one('res.partner','Partner')

class direccionext(models.Model):
    _inherit='res.partner'
#    office = fields.Char(string='Of/Local')
    office = fields.Selection([('oficina', 'Oficina'),('local', 'Local'),('lote', 'Lote'),('departamento', 'Departamento')])
    comuna = fields.Char(string='Comuna')
    birthdate_date = fields.Date("Cumpleaños")

    email_ids = fields.One2many('res.partner.dteemail', 'partner_id', string="Correos Facturacion")
    
    @api.onchange('comuna')
    def onchange_comuna(self):
        if self.comuna:
            try:
                search_prefix = 'CodigoPostal' # test prefix
                wsdl = "http://apicert.correos.cl:8008/ServicioNormalizacionExterno/cch/ws/distribucionGeografica/externo/implementacion/ServicioExternoNormalizacion.asmx?WSDL"
                client = Client(wsdl)
                datos = client.service.normalizarDireccion('PRUEBA WS 1','b9d591ae8ef9d36bb7d4e18438d6114e','61001',self.street,self.comuna)
                self.zip = datos.CodigoPostal
                self.city = (self.env["comunas.provincias"].search([("comuna", "=", self.comuna)])).provincia            
                estado = (self.env["comunas.provincias"].search([("comuna", "=", self.comuna)])).region
                print("estado : "+str(estado))
                self.state_id = (self.env["res.country.state"].search([("name", "=", estado)])).id
#                break
            except Exception:
                self.zip = 'Error'


    @api.onchange('function')
    def onchange_function(self):
        if self.function:
            self.function = self.function.title()

    @api.onchange('name')
    def onchange_name(self):
        if self.name:
            self.name = self.name.title()

    @api.onchange('phone')
    def onchange_phone(self):
        if self.phone:
            if self.country_id:
                if self.phone:
                    elnumero = phonenumbers.parse(self.phone, str(self.env['res.country'].search([('id','=',str(self.country_id.id))]).code))
                    if phonenumbers.is_valid_number(elnumero):
                        self.phone = phonenumbers.format_number(elnumero, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
                    else:
                        self.phone = ''
                        return {
                            'warning': {
                                'title': 'Error en número telefónico','message': 'Por favor ingresar número telefónico válido',
                                }
                            } 
            else:
                return {
                    'warning': {
                        'title': 'Falta Pais','message': 'Por favor ingresar direccion',
                        }
                    } 
                self.phone = ''

    @api.onchange('mobile')
    def onchange_mobile(self):
        if self.mobile:
            if self.country_id:
                if self.mobile:
                    elnumero = phonenumbers.parse(self.mobile, str(self.env['res.country'].search([('id','=',str(self.country_id.id))]).code))
                    if phonenumbers.is_valid_number(elnumero):
                        self.mobile = phonenumbers.format_number(elnumero, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
                    else:
                        self.mobile = ''
                        return {
                            'warning': {
                                'title': 'Error en número Movil','message': 'Por favor ingresar un número móvil válido',
                                }
                            } 
            else:
                return {
                    'warning': {
                        'title': 'Falta Pais','message': 'Por favor ingresar direccion',
                        }
                    }             
                self.mobile = ''

    @api.onchange('country_id')
    def onchange_country_id(self):
        if self.country_id:
            if self.phone:
                primero = self.phone
                segundo = primero[primero.find(" ")+1:len(primero)] 
                tercero = segundo.replace(" ","")
                self.phone = tercero

            if self.mobile:
                primero2 = self.phone
                segundo2 = primero2[primero2.find(" ")+1:len(primero2)] 
                tercero2 = segundo2.replace(" ","")
                self.mobile = tercero2

    @api.onchange('x_otro')
    def onchange_otro(self):
        print('otro')
