# -*- coding: utf-8 -*-
{
    'name': "Extension de Direccion",

    'summary': """
        Agregar Tipo de numeracion y comuna y codigo postal""",

    'description': """
        Agrega Tipo de Numeracion, Comuna, Provincia desde API de Google, ademas el codigo postal utilizando la API de normalizacion de Correos de Chile 
    """,

    'author': "Ivan Hills",
    'website': "http://www.dipromed.cl",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Partner',
    'version': '0.6',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}