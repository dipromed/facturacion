# -*- coding: utf-8 -*-
from odoo import models, fields

class miempaquetado(models.Model):
    _inherit = 'product.template'
    riskclass = fields.Selection(selection=[('na', 'N/A'),('1', 'I'),('2', 'II'), ('3', 'III'),('4', 'IV')] , default='na', string='Clasificación de Riesgo')
